﻿namespace Datus.Config
{
    using System;

    public interface ISqlCommandSet
    {
        /// <summary>
        /// Gets the SQL Command string from a custom app.config 
        /// configuration section for selecting particular entities
        /// </summary>
        string Select { get; }

        /// <summary>
        /// Gets the SQL Command string from a custom app.config 
        /// configuration section for inserting a particular entity
        /// </summary>
        string Insert { get; }

        /// <summary>
        /// Gets the SQL Command string from a custom app.config 
        /// configuration section for updating a particular entity
        /// </summary>
        string Update { get; }

        /// <summary>
        /// Gets the SQL Command string from a custom app.config 
        /// configuration section for deleting a particular entity
        /// </summary>
        string Delete { get; }

        /// <summary>
        /// Gets the SQL Command string from a custom app.config 
        /// configuration section for truncating particular entities
        /// </summary>
        string Truncate { get; }
    }
}
