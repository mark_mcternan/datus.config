﻿namespace Datus.Config
{
    public class SqlCommandSet : ISqlCommandSet
    {
        private readonly string _select;
        private readonly string _insert;
        private readonly string _update;
        private readonly string _delete;
        private readonly string _truncate;

        internal SqlCommandSet(SqlCommandSetSection configuration)
        {
            _select = configuration.Select.Value;
            _insert = configuration.Insert.Value;
            _update = configuration.Update.Value;
            _delete = configuration.Delete.Value;
            _truncate = configuration.Truncate.Value;
        }

        public string Select
        {
            get { return _select; }
        }

        public string Insert
        {
            get { return _insert; }
        }

        public string Update
        {
            get { return _update; }
        }

        public string Delete
        {
            get { return _delete; }
        }

        public string Truncate
        {
            get { return _truncate; }
        }
    }
}
