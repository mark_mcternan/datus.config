﻿namespace Datus.Config
{
    using System;
    using System.Configuration;

    public class SqlCommandSetSection : ConfigurationSection
    {
        public SqlCommandSetSection()
        {
        }

        public SqlCommandSetSection(string sectionName)
        {
            // load section from app.config
            var settings = ConfigurationManager.GetSection(sectionName) as SqlCommandSetSection;
            this.Select = this.TryGetElement(settings["select"] as CommandConfigurationElement);
            this.Insert = this.TryGetElement(settings["insert"] as CommandConfigurationElement);
            this.Update = this.TryGetElement(settings["update"] as CommandConfigurationElement);
            this.Delete = this.TryGetElement(settings["delete"] as CommandConfigurationElement);
            this.Truncate = this.TryGetElement(settings["truncate"] as CommandConfigurationElement);
        }

        [ConfigurationProperty("select", Options = ConfigurationPropertyOptions.None, IsRequired = false)]
        public CommandConfigurationElement Select
        {
            get { return (CommandConfigurationElement)this["select"]; }
            private set { this["select"] = value; }
        }

        [ConfigurationProperty("insert", IsRequired = false)]
        public CommandConfigurationElement Insert
        {
            get { return (CommandConfigurationElement) this["insert"]; }
            private set { this["insert"] = value; }
        }

        [ConfigurationProperty("update", IsRequired = false)]
        public CommandConfigurationElement Update
        {
            get { return (CommandConfigurationElement) this["update"]; }
            private set { this["update"] = value; }
        }

        [ConfigurationProperty("delete", IsRequired = false)]
        public CommandConfigurationElement Delete
        {
            get { return (CommandConfigurationElement) this["delete"]; }
            private set { this["delete"] = value; }
        }

        [ConfigurationProperty("truncate", IsRequired = false)]
        public CommandConfigurationElement Truncate
        {
            get { return (CommandConfigurationElement) this["truncate"]; }
            private set { this["truncate"] = value; }
        }

        /// <summary>
        /// Returns the named element if found and non-empty, otherwise returns null
        /// </summary>
        /// <param name="namedElement"></param>
        /// <returns></returns>
        private CommandConfigurationElement TryGetElement(CommandConfigurationElement namedElement)
        {
            return namedElement.Value != String.Empty ? (CommandConfigurationElement)namedElement
                                                      : new CommandConfigurationElement() { Value = null };
        }
    }
}
