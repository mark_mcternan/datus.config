﻿namespace Datus.Config
{
    using System.Collections.Generic;
    using System.Configuration;
    using System.Linq;
    using System.Reflection;


    public class CommandStrings
    {
        public const string DateFormat = "yyyy'-'MM'-'dd'T'HH':'mm':'ss'.'fff";

        // class vars - sql
        private static readonly ISqlCommandSet workitems;

        // class vars - misc.
        private static readonly Dictionary<string, PropertyInfo> cmdProperties;
        private static readonly List<string> resources;
        private static readonly string connection;

        /// <summary>
        /// Initializes the static members of the CommandStrings class
        /// </summary>
        static CommandStrings()
        {
            // load appsettings
            CommandStrings.connection = ConfigurationManager.ConnectionStrings["Datus.WorkQueue"].ConnectionString;
            // CommandStrings.resources = new List<string>(ConfigurationManager.AppSettings["SupportedResources"].Split(':'));

            // load SQL sections
            CommandStrings.workitems = new SqlCommandSet(new SqlCommandSetSection("SQLCommands/WorkItems.SQL"));

            // save set of properties which expose SQL Sections
            CommandStrings.cmdProperties = (from prop in typeof(CommandStrings).GetProperties(BindingFlags.Static | BindingFlags.Public | BindingFlags.GetProperty)
                                            where prop.PropertyType == typeof(ISqlCommandSet)
                                            select prop).ToDictionary(m => m.Name, m => m);
        }

        #region class attributes
        /// <summary>
        /// Gets the connection string for the current Database target
        /// </summary>
        public static string ConnectionString
        {
            get { return CommandStrings.connection; }
        }

        /// <summary>
        /// Gets the set of Resources defined in the app.config file
        /// </summary>
        //public static IEnumerable<string> SupportedResources
        //{
        //    get { return CommandStrings.resources; }
        //}

        #region SQL commandsets
        /// <summary>
        /// DEPENDENCY WARNING: By convention, SqlCommandSets must be in the form of "[Resource]"
        /// for the GetSQLFor(of T) method to work
        /// </summary>
        public static ISqlCommandSet WorkItems
        {
            get { return CommandStrings.workitems; }
        }
        #endregion SQL commandsets

        #endregion class attributes

        #region class methods
        /// <summary>
        /// Get a SQL command set for a given Domain object
        /// </summary>
        /// <typeparam name="T">The type of object to search for</typeparam>
        /// <returns>a set of sqlcommands if found, or null</returns>
        /// <remarks>WARNING: expects SqlCommandSet property names to be in the form of "[Resource]"</remarks>
        public static ISqlCommandSet GetSqlFor<T>()
        {
            var t = typeof(T);
            return CommandStrings.cmdProperties.ContainsKey(t.Name) ? CommandStrings.cmdProperties[t.Name].GetValue(null, null) as ISqlCommandSet : null;  
        }
        #endregion class methods
    }
}
