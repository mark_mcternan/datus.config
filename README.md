# README #

### What is this repository for? ###

* This code demonstrates how to setup custom configuration sections and commands in app.config [C# < 6] and expose them via a strongly typed objects

* Version 1.0

### How do I get set up? ###

* Pull down the project and explore the code

### Contribution guidelines ###

* None

### Who do I talk to? ###

* mark@datus.io