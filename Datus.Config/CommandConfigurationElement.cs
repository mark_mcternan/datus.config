﻿namespace Datus.Config
{
    using System;
    using System.Configuration;

    public class CommandConfigurationElement : ConfigurationElement
    {
        [ConfigurationProperty("value")]
        public string Value
        {
            get { return (string)this["value"]; }
            set { this["value"] = value; }
        }
    }
}

